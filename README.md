# React + TypeScript + Vite + Etherlink wallet connection

This template provides a minimal setup to get React working in Vite with Etherlink wallet connect and is hosted [here](https://etherlink-walletconnect.surge.sh/).
The wallet connection work with [WalletConnect](https://docs.walletconnect.com/web3modal/react/about) and [Wagmi](https://wagmi.sh/)

## Getting started

Before started, create WalletConnect project [here](https://cloud.walletconnect.com/app)
Copy paste your project-id in `src/App.tsx/ligne:38`.
The app won't work if you don't have a projectId.

## Run

Install all project dependencies :
```javascript
npm install
```

Run the project : 
```javascript
npm run dev
```
Visit your app [here](http://localhost:5173)



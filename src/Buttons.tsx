import { useAccount } from 'wagmi'
import { useWeb3Modal } from '@web3modal/wagmi/react'
import { sendTransaction } from '@wagmi/core'
import { parseEther } from 'viem'
import { config } from './App';

const Buttons = () => {
    const { open } = useWeb3Modal()
    const { address, isConnected } = useAccount()

    async function sendEth() {
        await sendTransaction(config,{
            to: '0xDEd399C85d29b284ab92fA16915eFcD9dEEd77b9',
            value: parseEther('0.01'),
        })
    }
    return (
        <>
            <button onClick={() => open()}>
                {isConnected ? `${address?.slice(0, 6)}...${address?.slice(-6, address.length)}` : 'Connect Wallet'}
            </button>
            <br />
            <br />
            <button onClick={() => open({ view: 'Networks' })}> Networks </button>
            <br />
            <br />
            <button onClick={async () => {
                await sendEth()
            }}> Test transfer </button>
        </>
    );
};

export default Buttons;

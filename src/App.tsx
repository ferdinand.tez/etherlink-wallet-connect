import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { createWeb3Modal } from "@web3modal/wagmi/react";
import { defaultWagmiConfig } from "@web3modal/wagmi/react/config";

import { WagmiProvider } from "wagmi";
import { etherlinkTestnet, etherlink } from "wagmi/chains";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { http } from "@wagmi/core";

// 0. Setup queryClient
const queryClient = new QueryClient();
import Buttons from "./Buttons";

// 1. Get projectId
const projectId = "06eb2796e25a6c3f45d93fa4481cf5a9";

// 2. Create wagmiConfig
const metadata = {
  name: "Etherlink",
  description: "Etherlink is a tezos EVM rollup.",
  url: "https://etherlink.com",
  icons: ["https://avatars.githubusercontent.com/u/37784886"],
};

const chains = [etherlinkTestnet, etherlink] as any;
export const config = defaultWagmiConfig({
  chains,
  auth: {
    email: true, // default to true
    socials: [
      "google",
      "x",
      "github",
      "discord",
      "apple",
      "facebook",
      "farcaster",
    ],
    showWallets: true, // default to true
    walletFeatures: true, // default to true
  },
  projectId,
  metadata,
  transports: {
    [etherlinkTestnet.id]: http(),
    [etherlink.id]: http(),
  },
});

// 3. Create modal
createWeb3Modal({
  metadata,
  wagmiConfig: config,
  projectId,
  enableAnalytics: true, // Optional - defaults to your Cloud configuration
});

function App() {
  return (
    <WagmiProvider config={config}>
      <QueryClientProvider client={queryClient}>
        <div>
          <a href="https://vitejs.dev" target="_blank">
            <img src={viteLogo} className="logo" alt="Vite logo" />
          </a>
          <a href="https://react.dev" target="_blank">
            <img src={reactLogo} className="logo react" alt="React logo" />
          </a>
        </div>
        <h1>Etherlink + Wagmi + Walletconnect</h1>
        <div className="card">
          <Buttons />
          <p>
            Edit <code>src/App.tsx</code> and save to test HMR
          </p>
        </div>
        <p className="read-the-docs">
          Click on the Vite and React logos to learn more
        </p>
      </QueryClientProvider>
    </WagmiProvider>
  );
}

export default App;
